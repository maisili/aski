#![crate_name = "aski"]
#![crate_type = "rlib"]

use {
  std::{ fs::read, path::Path },
  ron::de::{ Deserialize, from_str },
  serde::{ Serialize, Deserialize },
  hashbrown::HashMap,
  datosfiyr::Datosfiyr
};

#[derive( Serialize, Deserialize )]
pub struct Askiom {
  strok: Strok,
  sobstyns: Sobstyns,
}

#[derive( Serialize, Deserialize )]
struct Strok {
  djen: Djenyreicyn,
  spici: Spici,
}

#[derive( Serialize, Deserialize )]
pub enum Spici {
  DatomSpek,
}

#[derive( Serialize, Deserialize )]
enum Djenyreicyn {
  Dev,
}

#[derive( Serialize, Deserialize )]
struct Sobstyns {
  esyns: Esyns,
  orbit: Orbit,
  yfinitiz: Yfinitiz,
}

#[derive( Serialize, Deserialize )]
struct Esyns {}

#[derive( Serialize, Deserialize )]
struct Orbit {}

#[derive( Serialize, Deserialize )]
struct Yfinitiz {}

#[derive( Serialize, Deserialize )]
enum Eryr {
  Uops,
}

pub struct Askiosfiyr {}

impl Askiom { }

impl Askiosfiyr {
  fn krieitDatosfiyr(&self) -> Result<Datosfiyr, Eryr> {
    let datosfiyr = Datosfiyr { };
    Ok(datosfiyr)
  }
}

pub fn krieitDatoSfiyrFromAskiosfiyrPath(input: &Path)
  -> Result<Datosfiyr, Eryr>
{
  let askiosfiyr: Askiosfiyr = krieitAskiosfiyrFromPath(input)?;
  let datosfiyr: Datosfiyr = askiosfiyr.krieitDatosfiyr()?;
  Ok(datosfiyr)
}

fn krieitAskiosfiyrFromPath(input: &Path) -> Result<Askiosfiyr, Eryr>
{
  let baits: [u8] = read(input)?;
  let askiosfiyr = krieitAskiosfiyrfromBaits(&baits)?;
  Ok(askiosfiyr)
}

pub fn krieitAskiosfiyrfromBaits<'de, D> (baits: &'de [u8])
  -> Result<Askiom, Eryr>
{

}
